from django.shortcuts import render
from django.http import HttpResponse
from . import models
import requests

BASE_URL_ALPHA = "https://next.json-generator.com/api/json/get/41wV8bj_O"
BASE_URL_BRAVO = "https://next.json-generator.com/api/json/get/Nk48cbjdO"


def index(request):
    response_alpha = (requests.get(BASE_URL_ALPHA)).json()
    response_bravo = (requests.get(BASE_URL_BRAVO)).json()

    for item in response_alpha:
        _id = item['_id']
        mac = item['mac']
        guid = item['guid']
        interface = item['interface']
        address = item['address']
        new_hardware = models.HardwareInfo.create(ext_id=_id, mac=mac)
        new_interface = models.Interfaces.create(uuid=guid, hardware_info=new_hardware, name=interface, ipv4=address)
        new_hardware.save()
        new_interface.save()

    for i1 in response_bravo:

        id = i1['id']
        mac_address = i1['mac_address']
        new_hardware_2 = models.HardwareInfo.create(ext_id=id, mac=mac_address)
        new_hardware_2.save()

        for i2 in i1['interfaces']:
            iId = i2['id']
            iName = i2['name']
            iIpv4 = i2['ipv4']
            new_interface_2 = models.Interfaces.create(
                uuid=iId,
                hardware_info=new_hardware_2,
                name=iName,
                ipv4=iIpv4
            )
            new_interface_2.save()
    return HttpResponse(" localhost:8000/mi | localhost:8000/admin - u:admin p:admin")




