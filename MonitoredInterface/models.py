from django.db import models
from django.utils import timezone


class HardwareInfo(models.Model):
    external_id = models.CharField(max_length=100, primary_key=True) # PK
    mac_address = models.CharField(max_length=100)
    created_at = models.DateTimeField()
    modified_at = models.DateTimeField()

    @classmethod
    def create(cls, ext_id, mac):
        time_stamp = timezone.now()
        hardware = cls(
            external_id=ext_id,
            mac_address=mac,
            created_at=time_stamp,
            modified_at=time_stamp
        )
        return hardware

    @classmethod
    def update(cls, ext_id, mac):
        time_stamp = timezone.now()
        hardware = cls(
            external_id=ext_id,
            mac_address=mac,
            modified_at=time_stamp
        )
        return hardware


class Interfaces(models.Model):
    interface_uuid = models.CharField(primary_key=True, max_length=50)  # PK
    hw_info_fk = models.ForeignKey('HardwareInfo', on_delete=models.CASCADE)  # FK
    interface_name = models.CharField(" The name of the interface: ", max_length=20)
    ipv4_address = models.CharField("The ipv4 address of the interface: ", max_length=50)
    created_at = models.DateTimeField()
    modified_at = models.DateTimeField()

    @classmethod
    def create(cls, uuid, hardware_info, name, ipv4):
        time_stamp = timezone.now()
        interface = cls(
            interface_uuid=uuid,
            hw_info_fk=hardware_info,
            interface_name=name,
            ipv4_address=ipv4,
            created_at=time_stamp,
            modified_at=time_stamp
        )
        return interface

    @classmethod
    def update(cls, uuid, hardware_info, name, ipv4):
        time_stamp = timezone.now()
        interface = cls(
            interface_uuid=uuid,
            hw_info_fk=hardware_info,
            interface_name=name,
            ipv4_address=ipv4,
            modified_at=time_stamp
        )
        return interface
