from django.apps import AppConfig


class MonitoredInterfaceConfig(AppConfig):
    name = 'MonitoredInterface'
