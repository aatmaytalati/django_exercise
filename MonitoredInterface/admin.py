from django.contrib import admin

# Register your models here.

from .models import HardwareInfo
from .models import Interfaces

admin.site.register(HardwareInfo)
admin.site.register(Interfaces)